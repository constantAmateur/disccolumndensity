All the data and code necessary to generate [this paper](http://arxiv.org/abs/1207.6513) and all of its figures.
For the python code to work, you *must* have a few packages installed, such as
numpy and scipy (look at the imports at the top of pyInit.py).  Additionally, 
the code in makePlotsModule.py (which generates the most up to date figures) relies upon
the package mypy, available [here](https://bitbucket.org/constantAmateur/mypy).

The Spiral data directory contains the simulation data from Forgan et al 2011, provided by Ken Rice.
The Fragmented data directory contains unpublished simulation data provide by Peter Cossins and Giuseppe Lodato.
The other directories contain discs built with the parameters described in the
paper.  These were built using the R script buildDisc.R (with appropriate
parameters).  Converted to the Gadget2 binary format using
[EasyIC](https://bitbucket.org/constantAmateur/easyic).  Run through the
modified version of Gadget2, which outputs gravitational accelleration on its
own, which you can get [here](https://bitbucket.org/constantAmateur/gadgetoutputgravaccel), for one
tiny timestep to calculate the gravitational acceleration.  Finally, this
output was returned to ascii format using EasyIC, which is what is found in the
data directory.

Before building the paper with the tex file, make sure you run makePlotsModule.py to generate the
figures.

#The header block.  Defines everything else.
#The number of partciles in this file, listed from type 0 to 5
500000	1	0	0	0	0	
#Mass array.  Either gives the mass of each particle type or 0.0 if defined on a per/particle basis.
0	0	0	0	0	0	
#Time.  For initial conditions this field has no meaning
1.117587089538574E-07
#Redshift
0
#Star formation rate flag
0
#Feedback flag
0
#The total number of particles across all files for this simulation, for the 6 types
500000	1	0	0	0	0	
#Cooling flag
0
#Total number of files is this simulation
1
#The box size
10000
#Omega0
0.3
#OmegaLambda
0.7
#Hubble Parameter
0.7
#Star formation time flag
0
#Metals flag
0
#If the number of particles is 64 bits, this is the " high word" part of the total particle numbers
0	0	0	0	0	0	
#Is entropy used intstead of internal energy?
0
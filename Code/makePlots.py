import os
#Preamble
basedir=os.path.join(os.getcwd(),"..")
datadir=os.path.join(basedir,"Data")
figdir=os.path.join(basedir,"Figures")
fmt="pdf"
dpi=50
os.chdir(basedir)
execfile("Code/pyInit.py")

medium=AnalyticDisc()
medium.fromFolder(os.path.join(datadir,"MediumH"))
medium.describeDisc()
medium.interpolateMap()
medium.MMread("MM_MediumH_Output.txt")
medium.subseton("Agznostar")

massive=AnalyticDisc()
massive.fromFolder(os.path.join(datadir,"MassiveH"))
massive.describeDisc(T0=1670.,Mstar=.1,Mdisc=.01)
massive.interpolateMap()
massive.MMread("MM_MassiveH_Output.txt")
massive.subseton("Agznostar")

highres=AnalyticDisc()
highres.fromFolder(os.path.join(datadir,"HighRes"))
highres.describeDisc()
highres.interpolateMap()
highres.MMread("MM_HighRes_Output.txt")
highres.subseton("Agznostar")

spiral=Disc(G=1.)
spiral.fromFile(os.path.join(datadir,"Spiral/xyzmrho469.dat"))
spiral.fromBinaryFile(os.path.join(datadir,"Spiral/grav469"))
spiral.data=spiral.data[spiral.data['iphase']==0.,]
spiral.seedMap()
spiral.interpolateMap()
#Calculate an "exact" map to compare with interpolated one
spiral.seedMap(subsetFrac=1.)
spiral.subseton()
spiral.calcmidplaneCD()
spiral.calcsurCD()
spiral.addCol("gznostar")
spiral.data['gznostar']=spiral.data['gz']
#Calculate Sigma using Stamatellos method and add that to the plot.
nfact=.368
spiral.calcPotential()
spiral.addCol("StamSigma")
rho=spiral.data['rho']
spiral.data['StamSigma'][spiral.subset]=nfact*((spiral.fetch(spiral.data['Pot'])*spiral.fetch(rho))/(-4.*math.pi*spiral.G))**.5
#Add an inner radius to normalize to.  Choose the INITIAL inner radius
spiral.Rin=10.

#Figure 1
"""How accurately does the quantized disc reproduce the analytic one?  Plot the
ratio of Tree gravity to semi-analytic gravity against R."""

disc=medium
disc.ratioScatter(disc.data['R']/disc.Rin,'gznostar',"Agznostar", xlab="$R/R_i$", 
        ylab="$\log_2(g_z^{Tree}/g_z^{Analytic})$",
        title="Errors due to quantization\n(Thin disc)", newBands=True)
plt.xlim((-2.2,20.))
plt.ylim((-2.,2.))
plt.savefig(os.path.join(figdir,"Thin_Quantization."+fmt),format=fmt,dpi=dpi)

#Figure 2
disc=medium
"""Spatial distribution of the above."""

disc.plot(disc.data["R"]/disc.Rin,disc.data['z']/disc.Rin,box=False, xlab="$R/R_i$", ylab="$z/R_i$", title="Spatial distribution of errors due to quantization\n(Thin disc)")
plt.savefig(os.path.join(figdir,"Thin_QuantizationDist."+fmt),format=fmt,dpi=dpi)

#Figure 3
disc=massive


"""How accurately does the quantized disc reproduce the analytic one?  Plot the
ratio of Tree gravity to semi-analytic gravity against R."""

disc.ratioScatter(disc.data["R"]/disc.Rin,'gznostar',"Agznostar", xlab="$R/R_i$", 
        ylab="$\log_2(g_z^{Tree}/g_z^{Analytic})$",
        title="Errors due to quantization\n(Thick disc)", newBands=True)
plt.xlim((-2.2,20.))
plt.ylim((-2.,2.))
plt.savefig(os.path.join(figdir,"Thick_Quantization."+fmt),format=fmt,dpi=dpi)

#Figure 4
disc=massive
"""Spatial distribution of the above."""

disc.plot(disc.data["R"]/disc.Rin,disc.data['z']/disc.Rin,box=False, xlab="$R/R_i$", ylab="$z/R_i$", title="Spatial distribution of errors due to quantization\n(Thick disc)")
plt.savefig(os.path.join(figdir,"Thick_QuantizationDist."+fmt),format=fmt,dpi=dpi)

#Figure 5
disc=medium

"""How accurately does the infinite slab method reproduce the analytic
gravity?"""

disc.ratioScatter(disc.data["R"]/disc.Rin,4*math.pi*disc.G*disc.data['AcdMid'],"Agznostar",
        xlab="$R/R_i$",
        ylab="$\log_2(g_z^{Approx}/g_z^{Analytic})$", 
        title="Errors due to infinite slab approximation\n(Thin disc)", newBands=True)
plt.xlim((-2.2,20.))
plt.ylim((-1.,2.))
plt.savefig(os.path.join(figdir,"Thin_Slab."+fmt),format=fmt,dpi=dpi)


#Figure 6
disc=medium
"""Spatial"""

disc.plot(disc.data["R"]/disc.Rin,disc.data['z']/disc.Rin,box=False, xlab="$R/R_i$", ylab="$z/R_i$", title="Spatial distribution of errors due to slab approximation\n(Thin disc)")
plt.savefig(os.path.join(figdir,"Thin_SlabDist."+fmt),format=fmt,dpi=dpi)

#Figure 7
disc=massive

"""How accurately does the infinite slab method reproduce the analytic
gravity?"""

disc.ratioScatter(disc.data['R']/disc.Rin,4*math.pi*disc.G*disc.data['AcdMid'],"Agznostar",
        xlab="$R/R_i$",
        ylab="$\log_2(g_z^{Approx}/g_z^{Analytic})$", 
        title="Errors due to infinite slab approximation\n(Thick disc)", newBands=True)
plt.xlim((-2.2,20.))
plt.ylim((-1.,8.))
plt.savefig(os.path.join(figdir,"Thick_Slab."+fmt),format=fmt,dpi=dpi)


#Figure 8
disc=massive
"""Spatial"""

disc.plot(disc.data["R"]/disc.Rin,disc.data['z']/disc.Rin,box=False, xlab="$R/R_i$", ylab="$z/R_i$", title="Spatial distribution of errors due to slab approximation\n(Thick disc)")
plt.savefig(os.path.join(figdir,"Thick_SlabDist."+fmt),format=fmt,dpi=dpi)

#Figure 9

disc=medium
gold="AcdTot"
#gold="cdTot"
disc.ratioScatter(disc.data["R"]/disc.Rin, "cdTotInt", gold, xlab="$R/R_i$", 
        ylab="$\log_2(\Sigma_{map}/\Sigma_{Analytic})$",
        title="Errors in surface density map\n(Thin disc)", newBands=True)
plt.xlim((-2.2,20.))
plt.ylim((-1.,1.))
plt.savefig(os.path.join(figdir,"Thin_Surface."+fmt),format=fmt,dpi=dpi)

#Figure 10

disc=massive
gold="AcdTot"
#gold="cdTot"
disc.ratioScatter(disc.data["R"]/disc.Rin, "cdTotInt", gold, xlab="$R/R_i$", 
        ylab="$\log_2(\Sigma_{map}/\Sigma_{Analytic})$",
        title="Errors in surface density map\n(Thick disc)", newBands=True)
plt.xlim((-2.2,20.))
plt.ylim((-1.,1.))
plt.savefig(os.path.join(figdir,"Thick_Surface."+fmt),format=fmt,dpi=dpi)

#Figure 11

first=medium
second=highres

#Plot the data points...

a=first.ratioScatter(first.data["R"]/first.Rin,'gznostar',"Agznostar", xlab="$R/R_i$", 
        ylab="$\log_2(g_z^{Tree}/g_z^{Analytic})$",
        title="Effect of resolution on errors due to quantization", useBands=False, box=False,pch=1)

b=second.ratioScatter(second.data["R"]/first.Rin,"gznostar","Agznostar", useBands=False, add=True,
        box=False,pch=2)


#Draw the boxplots 
x=first.fetch(first.data['R']/first.Rin)
y1=np.log2(np.abs(first.fetch(first.data['gznostar']))/np.abs(first.fetch(first.data['Agznostar'])))
y2=np.log2(np.abs(second.fetch(second.data['gznostar']))/np.abs(second.fetch(second.data['Agznostar'])))

qs1=np.percentile(y1,[5,25,50,75,95])
qs2=np.percentile(y2,[5,25,50,75,95])
whis1=np.max((qs1[4]-qs1[2],qs1[2]-qs1[0]))/(qs1[3]-qs1[1])
whis2=np.max((qs2[4]-qs2[2],qs2[2]-qs2[0]))/(qs2[3]-qs2[1])
xsize=sround((x.max()-x.min())/10.)
plt.boxplot(y1,positions=[x.min()-2.5*xsize],widths=[xsize],whis=whis1)
plt.boxplot(y2,positions=[x.min()-xsize],widths=[xsize],whis=whis2)

#Finally, add the ticks..
pylab.xticks(np.arange(sround(x.min()),sround(x.max()),xsize))
#And a legend...
plt.legend([a[1][0][0],b[1][0][0]],["LowRes","HighRes"],loc="upper right")
plt.xlim((-5.2,20.))
plt.ylim((-2.,2.))

plt.savefig(os.path.join(figdir,"Resolution."+fmt),format=fmt,dpi=dpi)

#Figure 12
disc=medium
gold="AcdSur"
#gold="cdSur"

disc.ratioScatter(disc.data["R"]/disc.Rin, disc.data['cdTotInt']-disc.data["gznostar"]/(-4.*math.pi*disc.G*np.sign(disc.data['z'])),
        gold, xlab="$R/R_i$",
        ylab="$\log_2(\Sigma_{estim}/\Sigma_{Analytic})$", 
        title="Errors in estimation of $\Sigma$\n(Thin disc)", newBands=True)
plt.xlim((-2.2,20.))
plt.ylim((-4.,4.))
plt.savefig(os.path.join(figdir,"Thin_Sigma."+fmt),format=fmt,dpi=dpi)

#Figure 13
disc=medium
"""Spatial"""

disc.plot(disc.data["R"]/disc.Rin,disc.data['z']/disc.Rin,box=False, xlab="$R/R_i$", ylab="$z/R_i$", 
        title="Spatial distribution of errors in $\Sigma$\n(Thin disc)")
plt.savefig(os.path.join(figdir,"Thin_SigmaDist."+fmt),format=fmt,dpi=dpi)

#Figure 14
disc=spiral

#gold="AcdMid"
gold="cdMid"
disc.ratioScatter(disc.data["R"]/1., disc.data["gznostar"]/(-4.*math.pi*disc.G*np.sign(disc.data['z'])),
        gold, xlab="R [AU]",
        ylab="$\log_2(\Sigma^'_{estim}/\Sigma^'_{Analytic})$", 
        title="Errors in estimation of $\Sigma^'$", newBands=True)
plt.xlim((-30.,120.))
plt.ylim((-2.,2.))
plt.savefig(os.path.join(figdir,"Spiral_Sigmap."+fmt),format=fmt,dpi=dpi)

#Figure 15
disc=spiral
"""Spatial"""

disc.plot(disc.data["R"]/1.,disc.data['z']/1.,box=False, xlab="R [AU]", ylab="z [AU]", 
        title="Spatial distribution of errors in $\Sigma^'$")
plt.xlim((0.,120.))
plt.ylim((-8.,8.))
plt.savefig(os.path.join(figdir,"Spiral_SigmapDist."+fmt),format=fmt,dpi=dpi)

#Figure 16
disc=spiral
"""Accuracey of Sigma"""

#gold="AcdSur"
gold="cdSur"

disc.ratioScatter(disc.data["R"]/1., disc.data['cdTotInt']-disc.data["gznostar"]/(-4.*math.pi*disc.G*np.sign(disc.data['z'])),
        gold, xlab="R [AU]",
        ylab="$\log_2(\Sigma_{estim}/\Sigma_{Analytic})$", 
        title="Errors in estimation of $\Sigma$", newBands=True)

disc.ratioScatter(disc.data["R"]/1.,"StamSigma",gold,add=True,color='black',useBands=False,newBands=False,pch='*')
plt.xlim((-30.,120.))
plt.ylim((-8.,8.))

plt.savefig(os.path.join(figdir,"Spiral_Sigma."+fmt),format=fmt,dpi=dpi)

#Figure 17
disc=spiral
"""Spatial"""

disc.plot(disc.data["R"]/1.,disc.data['z']/1.,box=False, xlab="R [AU]", ylab="z [AU]", 
        title="Spatial distribution of errors in $\Sigma$")
plt.xlim((0.,120.))
plt.ylim((-8.,8.))
plt.savefig(os.path.join(figdir,"Spiral_SigmaDist."+fmt),format=fmt,dpi=dpi)

#Figure 18
disc=spiral
fig=plt.figure(figsize=(20,40))
fig.subplots_adjust(hspace=.3)
fig.add_subplot(211)
o=np.where((np.abs(disc.data['x'])<45) & (np.abs(disc.data['y'])<45))
heatMap(disc.data['x'][o]/1.,disc.data['y'][o]/1.,disc.data['cdTotInt'][o],title="Surface density map\n(Approximate)", xlab="x [AU]",ylab="y [AU]",bar=True)
plt.xlim((-45.,45.))
plt.ylim((-45.,45.))
fig.add_subplot(212)
heatMap(disc.data['x'][o]/1.,disc.data['y'][o]/1.,disc.data['cdTot'][o],title="Surface density map\n(Exact)", xlab="x [AU]",ylab="y [AU]",bar=True)
plt.xlim((-45.,45.))
plt.ylim((-45.,45.))
plt.savefig(os.path.join(figdir,"Spiral_SurfaceDensity."+fmt),format=fmt,dpi=dpi)

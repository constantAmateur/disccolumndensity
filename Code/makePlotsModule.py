#Passed all tests with version 0.3.28 of mypy...
#Assumed it has been launched from the directory where the code is kept.  If
#not, modify basedir accordingly.
import os
import numpy as np
from mypy import data, plot
from mypy.math import sround
from mypy.plot import plt
import matplotlib as mpl
#Preamble
basedir=os.path.join(os.getcwd(),'..')
datadir=os.path.join(basedir,"Data")
figdir=os.path.join(basedir,"Figures")
ptCmap=mpl.cm.Oranges
ptSize=5
linewidths=.2
#Note only the plot area will be rasterized...
raster=True
fmt="pdf"
dpi=300
#Set the default figure parameters...
#This will produce figures of the size we'll use in the paper...
mpl.rcParams['figure.figsize']=[3.15,2.3625]
mpl.rcParams['legend.numpoints']=4
mpl.rcParams['font.size']=8.
mpl.rcParams['axes.linewidth']=.3
mpl.rcParams['lines.linewidth']=.3
mpl.rcParams['grid.linewidth']=.3
mpl.rcParams['patch.linewidth']=.3

os.chdir(basedir)
medium=data.AnalyticDisc()
medium.fromFolder(os.path.join(datadir,"MediumH"))
medium.describeDisc()
medium.interpolateMap()
medium.MMread("MM_MediumH_Output.txt")
medium.subsetSet("Agznostar")

massive=data.AnalyticDisc()
massive.fromFolder(os.path.join(datadir,"MassiveH"))
massive.describeDisc(T0=1670.,Mstar=.1,Mdisc=.01)
massive.interpolateMap()
massive.MMread("MM_MassiveH_Output.txt")
massive.subsetSet("Agznostar")

highres=data.AnalyticDisc()
highres.fromFolder(os.path.join(datadir,"HighRes"))
highres.describeDisc()
highres.interpolateMap()
highres.MMread("MM_HighRes_Output.txt")
highres.subsetSet("Agznostar")

spiral=data.Disc()
spiral.G=1.
spiral.fromFile(os.path.join(datadir,"Spiral/xyzmrho469.dat"))
spiral.fromBinaryFile(os.path.join(datadir,"Spiral/grav469"))
#Throw out the star and the accreted particles...
spiral.delParticles(np.where(spiral.getCol('iphase')!=0.)[0])
spiral.seedMap()
spiral.interpolateMap()
##Calculate an "exact" map to compare with interpolated one
spiral.seedMap(subsetFrac=1.)
spiral.subsetSet()
spiral.calcmidplaneCD()
spiral.calcsurCD()
spiral.gznostar_=spiral.gz_
##Calculate Sigma using Stamatellos method and add that to the plot.
nfact=.368
spiral.calcPotential()
spiral.StamSigma_=nfact*((spiral.Pot_*spiral.rho_)/(-4*np.pi*spiral.G))**.5
#spiral.data['StamSigma'][spiral.subset]=nfact*((spiral.fetch(spiral.data['Pot'])*spiral.fetch(rho))/(-4.*math.pi*spiral.G))**.5
##Add an inner radius to normalize to.  Choose the INITIAL inner radius
#spiral.Rin=10.
#
spiral.subsetSet(["cdSur","cdMid"])

os.chdir(basedir)
frag=data.Disc()
frag.G=1.
frag.fromFile(os.path.join(datadir,"Fragmented/ALMA314.dat"),format="x y z m h rho vx vy vz u gradh gradsoft iphase")
frag.fromBinaryFile(os.path.join(datadir,"Fragmented/grav314"))
#Throw out everything but gas and those that are too far out...
frag.delParticles(np.where(frag.getCol('iphase')!=1.)[0])
frag.delParticles(np.where((np.abs(frag.getCol('x'))>50))[0])
frag.delParticles(np.where((np.abs(frag.getCol('y'))>50))[0])
#frag.subsetSet()
#frag.calcsumgz()
#frag.gz_=frag.sumgz_
#frag.subsetSet(subset=None)
#Now the painful part...
frag.seedMap()
frag.interpolateMap()
###Calculate an "exact" map for comparison
#frag.seedMap(subsetFrac=1.)
#frag.subsetSet(fields="sumgz")
#frag.calcmidplaneCD()
#frag.calcsurCD()
#frag.gznostar_=frag.gz_
#Focus on one of the fragments...
frag.loc=[(-39.3843,15.1903)]
fragRadius=.25
frag.subsetSet(subset=None)
frag.fragR_=np.sqrt((frag.x_-frag.loc[0][0])**2+(frag.y_-frag.loc[0][1])**2)
o=np.where(frag.fragR_<fragRadius)[0]
frag.subsetSet(subset=o)
frag.calcsumgz()
frag.gznostar_=frag.sumgz_
frag.calcmidplaneCD()
frag.calcsurCD()
#Calculate Sigma using Stamatellos method and add that to the data
nfact=.368
frag.calcPotential()
frag.StamSigma_=nfact*((frag.Pot_*frag.rho_)/(-4*np.pi*frag.G))**.5



#Figure 1
"""How accurately does the quantized disc reproduce the analytic one?  Plot the
ratio of Tree gravity to semi-analytic gravity against R."""

disc=medium
fig=disc.ratioScatter(disc.R_/disc.Rin,disc.gznostar_,disc.Agznostar_,newBands=True,maxVal=2,
        box=True, xlab="$R/R_i$", 
        ylab="$\log_2(g_z^{oct-tree}/g_z^{Continuous})$",
        title="Errors due to quantization\n(Thin disc)",
        cmap=ptCmap,s=ptSize,rasterized=raster,linewidths=linewidths)
fig[-1].set_label("$|\log_2(g_z^{oct-tree}/g_z^{Continuous})|$")
plt.xlim((-2.2,20.))
plt.ylim((-2.,2.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Thin_Quantization."+fmt),format=fmt,dpi=dpi)

#Figure 2
disc=medium
"""Spatial distribution of the above."""

fig=disc.plot(disc.R_/disc.Rin,disc.z_/disc.Rin, xlab="$R/R_i$", ylab="$z/R_i$",
        title="Spatial distribution of errors due \nto quantization (Thin disc)",
        cmap=ptCmap,s=ptSize,rasterized=raster,linewidths=linewidths)
fig[-1].set_label("$|\log_2(g_z^{oct-tree}/g_z^{Continuous})|$")
plt.xlim((0,20))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Thin_QuantizationDist."+fmt),format=fmt,dpi=dpi)

#Figure 3
disc=massive

"""How accurately does the quantized disc reproduce the analytic one?  Plot the
ratio of Tree gravity to semi-analytic gravity against R."""
fig=disc.ratioScatter(disc.R_/disc.Rin,disc.gznostar_,disc.Agznostar_,newBands=True,maxVal=2,
        box=True, xlab="$R/R_i$", 
        ylab="$\log_2(g_z^{oct-tree}/g_z^{Continuous})$",
        title="Errors due to quantization\n(Thick disc)",
        cmap=ptCmap,s=ptSize,rasterized=raster,linewidths=linewidths)
fig[-1].set_label("$|\log_2(g_z^{oct-tree}/g_z^{Continuous})|$")
plt.xlim((-2.2,20.))
plt.ylim((-2.,2.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Thick_Quantization."+fmt),format=fmt,dpi=dpi)

#Figure 4
disc=massive
"""Spatial distribution of the above."""
fig=disc.plot(disc.R_/disc.Rin,disc.z_/disc.Rin, xlab="$R/R_i$", ylab="$z/R_i$",
        title="Spatial distribution of errors due \nto quantization (Thick disc)",
        cmap=ptCmap,s=ptSize,rasterized=raster,linewidths=linewidths)
fig[-1].set_label("$|\log_2(g_z^{oct-tree}/g_z^{Continuous})|$")
plt.xlim((0,20.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Thick_QuantizationDist."+fmt),format=fmt,dpi=dpi)

#Figure 5
disc=medium

"""How accurately does the infinite slab method reproduce the analytic
gravity?"""
fig=disc.ratioScatter(disc.R_/disc.Rin,4*np.pi*disc.G*disc.AcdMid_,disc.Agznostar_,
        newBands=True,maxVal=2,
        box=True, xlab="$R/R_i$", 
        ylab="$\log_2(g_z^{Slab}/g_z^{Continuous})$",
        title="Errors due to infinite slab approximation\n(Thin disc)",
        cmap=ptCmap,s=ptSize,rasterized=raster,linewidths=linewidths)
fig[-1].set_label("$|\log_2(g_z^{Slab}/g_z^{Continuous})|$")
plt.xlim((-2.2,20.))
plt.ylim((-1.,2.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Thin_Slab."+fmt),format=fmt,dpi=dpi)


#Figure 6
disc=medium
"""Spatial"""

fig=disc.plot(disc.R_/disc.Rin,disc.z_/disc.Rin, xlab="$R/R_i$", ylab="$z/R_i$",
        title="Spatial distribution of errors due to slab \napproximation (Thin disc)",
        cmap=ptCmap,s=ptSize,rasterized=raster,linewidths=linewidths)
plt.xlim((0,20.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
fig[-1].set_label("$|\log_2(g_z^{Slab}/g_z^{Continuous})|$")
plt.savefig(os.path.join(figdir,"Thin_SlabDist."+fmt),format=fmt,dpi=dpi)

#Figure 7
disc=massive

"""How accurately does the infinite slab method reproduce the analytic
gravity?"""
fig=disc.ratioScatter(disc.R_/disc.Rin,4*np.pi*disc.G*disc.AcdMid_,disc.Agznostar_,
        newBands=True,maxVal=8,
        box=True, xlab="$R/R_i$", 
        ylab="$\log_2(g_z^{Slab}/g_z^{Continuous})$",
        title="Errors due to infinite slab approximation\n(Thick disc)",
        cmap=ptCmap,s=ptSize,rasterized=raster,linewidths=linewidths)
fig[-1].set_label("$|\log_2(g_z^{Slab}/g_z^{Continuous})|$")
plt.xlim((-2.2,20.))
plt.ylim((-1.,8.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Thick_Slab."+fmt),format=fmt,dpi=dpi)


#Figure 8
disc=massive
"""Spatial"""
fig=disc.plot(disc.R_/disc.Rin,disc.z_/disc.Rin, xlab="$R/R_i$", ylab="$z/R_i$",
        title="Spatial distribution of errors due to slab \napproximation (Thick disc)",
        cmap=ptCmap,s=ptSize,rasterized=raster,linewidths=linewidths)
plt.xlim((0,20.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
fig[-1].set_label("$|\log_2(g_z^{Slab}/g_z^{Continuous})|$")

plt.savefig(os.path.join(figdir,"Thick_SlabDist."+fmt),format=fmt,dpi=dpi)

#Figure 9

disc=medium
gold="AcdTot"
#gold="cdTot"
fig=disc.ratioScatter(disc.R_/disc.Rin, "cdTotInt", gold, xlab="$R/R_i$", 
        ylab="$\log_2(\Sigma_{map}/\Sigma_{Continuous})$",
        title="Errors in total surface density map\n(Thin disc)", newBands=True,
        maxVal=1, box=True, cmap=ptCmap, s=ptSize, rasterized=raster,linewidths=linewidths)
fig[-1].set_label("$|\log_2(\Sigma_{map}/\Sigma_{Continuous})|$")
plt.xlim((-2.2,20.))
plt.ylim((-1.,1.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Thin_Surface."+fmt),format=fmt,dpi=dpi)

#Figure 10

disc=massive
gold="AcdTot"
#gold="cdTot"
fig=disc.ratioScatter(disc.R_/disc.Rin, "cdTotInt", gold, xlab="$R/R_i$", 
        ylab="$\log_2(\Sigma_{map}/\Sigma_{Continuous})$",
        title="Errors in total surface density map\n(Thick disc)", newBands=True,
        maxVal=1, box=True, cmap=ptCmap, s=ptSize, rasterized=raster,linewidths=linewidths)
fig[-1].set_label("$|\log_2(\Sigma_{map}/\Sigma_{Continuous})|$")
plt.xlim((-2.2,20.))
plt.ylim((-1.,1.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Thick_Surface."+fmt),format=fmt,dpi=dpi)

#Figure 11

first=highres
second=medium

#Plot the data points...
fig=first.ratioScatter(first.R_/first.Rin,first.gznostar_,first.Agznostar_,xlab="$R/R_i$",
        ylab="$\log_2(g_z^{oct-tree}/g_z^{Continuous})$",
        title="Effect of resolution on errors \ndue to quantization",
        useBands=False, box=True,c='darkred',rasterized=raster, s=ptSize,linewidths=linewidths)
#Add the second lot of datapoints
secondPts=second.ratioScatter(second.R_/second.Rin,second.gznostar_,second.Agznostar_,
        useBands=False, add=fig[0].axes[0], box=False, s=ptSize, c='white',
        alpha=.4,linewidths=linewidths)
#Add the other boxplot
x=second.R_/second.Rin
y=np.log2(np.abs(second.gznostar_)/np.abs(second.Agznostar_))
qs=np.percentile(y,[5,25,50,75,95])
whis=np.max((qs[4]-qs[2],qs[2]-qs[0]))/(qs[3]-qs[1])
xsize=sround((x.max()-x.min())/10.)
ax=fig[0].axes[0]
boxobj=plot.trueBoxplot(y,positions=[ax.xaxis.get_data_interval()[0]-xsize], 
        widths=[xsize],tgt=ax,sym='')
#Restore axis to something sensible.
tmp=ax.xaxis.get_data_interval()
ax.xaxis.set_view_interval(tmp[0],tmp[1])
ax.xaxis.set_major_locator(mpl.ticker.AutoLocator())
ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
#Add a legend
plt.legend([fig[-1],secondPts[-1]],["HighRes","LowRes"],loc="upper right")
plt.xlim((-5.2,20.))
plt.ylim((-2.,2.))
plt.subplots_adjust(bottom=.17,left=.21,right=.9,top=.82)
#Save
plt.savefig(os.path.join(figdir,"Resolution."+fmt),format=fmt,dpi=dpi)

#Figure 12
disc=medium
gold="AcdSur"
#gold="cdSur"

fig=disc.ratioScatter(disc.R_/disc.Rin,
        disc.cdTotInt_-disc.gznostar_/(-4.*np.pi*disc.G*np.sign(disc.z_)),
        gold, xlab="$R/R_i$",
        ylab="$\log_2(\Sigma_{estim}/\Sigma_{Continuous})$", 
        title="Errors in estimation of $\Sigma$\n(Thin disc)", newBands=True,
        maxVal=4,cmap=ptCmap, s=ptSize, rasterized=raster, box=True,linewidths=linewidths)
fig[-1].set_label("$|\log_2(\Sigma_{estim}/\Sigma_{Continuous})|$")
plt.xlim((-2.2,20.))
plt.ylim((-4.,4.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Thin_Sigma."+fmt),format=fmt,dpi=dpi)

#Figure 13
disc=medium
"""Spatial"""

fig=disc.plot(disc.R_/disc.Rin,disc.z_/disc.Rin, xlab="$R/R_i$", ylab="$z/R_i$", 
        title="Spatial distribution of errors in $\Sigma$\n(Thin disc)",
        cmap=ptCmap, s=ptSize, rasterized=raster,linewidths=linewidths)
fig[-1].set_label("$|\log_2(\Sigma_{estim}/\Sigma_{Continuous})|$")
plt.xlim((0,20.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Thin_SigmaDist."+fmt),format=fmt,dpi=dpi)

#Figure 14
disc=spiral

#gold="AcdMid"
gold="cdMid"
fig=disc.ratioScatter(disc.R_, disc.gznostar_/(-4.*np.pi*disc.G*np.sign(disc.z_)),
        gold, xlab="R [AU]",
        ylab="$\log_2(\Sigma^{'}_{estim}/\Sigma^{'}_{count})$", 
        title="Errors in estimation of $\Sigma^{'}$", newBands=True, maxVal=2,
        cmap=ptCmap, s=ptSize, rasterized=raster, box=True,linewidths=linewidths)
fig[-1].set_label("$|\log_2(\Sigma^{'}_{estim}/\Sigma^{'}_{count})|$")
plt.xlim((-30.,120.))
plt.ylim((-2.,2.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Spiral_Sigmap."+fmt),format=fmt,dpi=dpi)

#Figure 15
disc=spiral
"""Spatial"""

fig=disc.plot(disc.R_,disc.z_, xlab="R [AU]", ylab="z [AU]", 
        title="Spatial distribution of \nerrors in $\Sigma^{'}$", cmap=ptCmap,
        s=ptSize, rasterized=raster,linewidths=linewidths)
fig[-1].set_label("$|\log_2(\Sigma^{'}_{estim}/\Sigma^{'}_{count})|$")
plt.xlim((0.,120.))
plt.ylim((-8.,8.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Spiral_SigmapDist."+fmt),format=fmt,dpi=dpi)

#Figure 16
disc=spiral
"""Accuracey of Sigma"""

#gold="AcdSur"
gold="cdSur"

fig=disc.ratioScatter(disc.R_,
        disc.cdTotInt_-disc.gznostar_/(-4.*np.pi*disc.G*np.sign(disc.z_)),
        gold, xlab="R [AU]",
        ylab="$\log_2(\Sigma_{estim}/\Sigma_{count})$", 
        title="Errors in estimation of $\Sigma$", newBands=True, cmap=ptCmap,
        s=ptSize, rasterized=raster, box=True,maxVal=3.,linewidths=linewidths)
fig[-1].set_label("$|\log_2(\Sigma_{estim}/\Sigma_{count})|$")
stam=disc.ratioScatter(disc.R_,"StamSigma", gold, color='black',
        useBands=False, newBands=False, add=fig[0].axes[0], pch='x', s=ptSize
        ,linewidths=linewidths, alpha=.4)
plt.xlim((-30.,120.))
plt.ylim((-3.,5.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)

plt.savefig(os.path.join(figdir,"Spiral_Sigma."+fmt),format=fmt,dpi=dpi)

#Figure 17
disc=spiral
"""Spatial"""

fig=disc.plot(disc.R_,disc.z_, xlab="R [AU]", ylab="z [AU]", 
        title="Spatial distribution of \nerrors in $\Sigma$", cmap=ptCmap,
        s=ptSize, rasterized=raster,linewidths=linewidths)
fig[-1].set_label("$|\log_2(\Sigma_{estim}/\Sigma_{count})|$")
plt.xlim((0.,120.))
plt.ylim((-8.,8.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Spiral_SigmaDist."+fmt),format=fmt,dpi=dpi)

#Figure 18
#mpl.rcParams['figure.figsize']=[3.15,2.3625]

disc=spiral
fig=plt.figure(figsize=(3.15,4.725))
fig.subplots_adjust(hspace=.5)
disc.subsetTmp([(0,None)])
o=np.where((np.abs(disc.getCol('x'))<45) & (np.abs(disc.getCol('y'))<45))[0]
disc.subsetSet(subset=disc.gInd2sInd(o))
t=plot.heatMap(disc.x_,disc.y_,disc.cdTotInt_, title="Total surface density map\n(Approximate)", 
        xlab="x [AU]", ylab="y [AU]", bar=True,subplot=211,
        rasterized=raster,fig=fig,vmin=-6.0,vmax=-4.0)
t[-1].set_label("$log_{10}(\Sigma)$")
plt.xlim((-45.,45.))
plt.ylim((-45.,45.))
t=plot.heatMap(disc.x_,disc.y_,disc.cdTot_, title="Total surface density map\n(Exact)", 
        xlab="x [AU]", ylab="y [AU]", bar=True,subplot=212,
        rasterized=raster,fig=fig,vmin=-6.0,vmax=-4.0)
t[-1].set_label("$log_{10}(\Sigma)$")
plt.xlim((-45.,45.))
plt.ylim((-45.,45.))
plt.savefig(os.path.join(figdir,"Spiral_SurfaceDensity."+fmt),format=fmt,dpi=dpi)

#Figure 19

disc=frag

disc.subsetSet(subset=None)
fig=plot.heatMap(disc.x_,disc.y_,disc.cdTotInt_,title="Total surface density map\n(Approximate)",
    xlab="x [AU]", ylab="y [AU]", bar=True, rasterized=raster)
fig[-1].set_label("$log_{10}(\Sigma)$")
plt.xlim((-50.,50.))
plt.ylim((-50.,50.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Frag_SurfaceDensity."+fmt),format=fmt,dpi=dpi)

#Figure 20

disc=frag

disc.subsetSet(subset=None)
o=np.where(disc.fragR_<fragRadius)[0]
disc.subsetSet(subset=o)

fig=disc.ratioScatter(disc.fragR_,
        disc.cdTotInt_-disc.gznostar_/(-4.*np.pi*disc.G*np.sign(disc.z_)),
        "cdSur", xlab="$R_{frag}$ [AU]",
        ylab="$\log_2(\Sigma_{estim}/\Sigma_{count})$", 
        title="Errors in estimation of $\Sigma$", newBands=True, cmap=ptCmap,
        s=ptSize, rasterized=raster, box=True,maxVal=4.,linewidths=linewidths)
fig[-1].set_label("$|\log_2(\Sigma_{estim}/\Sigma_{count})|$")
stam=disc.ratioScatter(disc.fragR_,"StamSigma", "cdSur", color='black',
        useBands=False, newBands=False, add=fig[0].axes[0], pch='x', s=ptSize
        ,linewidths=linewidths, alpha=.4)
plt.xlim((-.05,.25))
plt.ylim((-2,5.5))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Frag_Sigma."+fmt),format=fmt,dpi=dpi)

#Figure 21
disc=frag
"""Spatial"""

fig=disc.plot(disc.fragR_,disc.z_, xlab="$R_{frag}$ [AU]", ylab="z [AU]", 
        title="Spatial distribution of \nerrors in $\Sigma$", cmap=ptCmap,
        s=ptSize, rasterized=raster,linewidths=linewidths)
fig[-1].set_label("$|\log_2(\Sigma_{estim}/\Sigma_{count})|$")
plt.xlim((0.,.25))
plt.ylim((-2.,2.))
plt.subplots_adjust(bottom=.17,left=.21,right=.76,top=.82)
plt.savefig(os.path.join(figdir,"Frag_SigmaDist."+fmt),format=fmt,dpi=dpi)


